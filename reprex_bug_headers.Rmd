---
title: "reprex_bug_headers"
subtitle: "subtitle"
author: 
  - "Some Author"
classname: "Class"
formation: "Some master degree"
parcours: 'about something'
notesA: "People A"
notesB: "People B"

date: "`r Sys.Date()`"
output:
  pdf_document:
    template: template/etude.tex
    toc: true
    number_sections: true
    #keep_tex : true
    latex_engine: xelatex
    fig_width: 7
    fig_height: 6
    fig_caption: true
    includes:
    #  in_header: /data/templates/chunks/header.tex
    #  before_body: /data/templates/chunks/doc_prefix.tex
    #  after_body: appendices.md
mainfont: Linux Libertine O
sansfont: Linux Biolinum O
maincolor: rstudioblue
secondcolor: bleuciel
mainimage: images/FlyingLifterv2.png
logo2: images/Rlogo.png
logo1: images/Rlogo.png
fontsize: 11pt
lang: fr
tocname: "Sommaire"
lof: yes
lot: yes
---


\clearpage

# Abstract {-}

\lipsum[1]

\clearpage

```{r child = 'fichiers/00_introduction.Rmd'}
```


```{r child = 'fichiers/01_chapter_1.Rmd'}
```



```{r child = 'fichiers/02_chapter_2.Rmd'}
```


```{r child = 'fichiers/annexes.Rmd'}
```
